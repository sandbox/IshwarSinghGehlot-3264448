<?php
/**
 * @file
 * Theme settings in this file.
 */
use Drupal\core\Form;
use Drupal\core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;


/**
 * Implements hook_form_system_theme_settings_alter().
 */

function decor_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {

    $form['decor_settings'] = [
        '#type' => 'vertical_tabs',
        '#title' => new TranslatableMarkup('Decor Settings'),
    ];

    $form['basic'] = [
        '#type' => 'details',
        '#group' => 'Decor_settings',
        '#title' => new TranslatableMarkup('Basic'),
    ];
    $form['basic']['type_theme'] = [
        '#type' => 'select',
        '#title' => new TranslatableMarkup('Select Theme'),
        '#default_value' => theme_get_setting('type_theme'),
        '#options' => [
            'light' => new TranslatableMarkup("Light Theme"),
            'dark' => new TranslatableMarkup("Dark Theme"),
        ],
    ];
    $form['content'] = [
        '#type' => 'details',
        '#group' => 'decor_settings',
        '#title' => new TranslatableMarkup('Content'),
    ];
    $form['content']['401_content'] = [
        '#type' => 'text_format',
        '#format' => 'full_html',
        '#title' => new TranslatableMarkup('401 Content.'),
        '#default_value' => theme_get_setting('401_content')['value'],
    ];
    $form['content']['403_content'] = [
        '#type' => 'text_format',
        '#format' => 'full_html',
        '#title' => new TranslatableMarkup('403 Content.'),
        '#default_value' => theme_get_setting('403_content')['value'],
    ];
    $form['content']['404_content'] = [
        '#type' => 'text_format',
        '#format' => 'full_html',
        '#title' => new TranslatableMarkup('404 Content.'),
        '#default_value' => theme_get_setting('404_content')['value'],
    ]; 

}
